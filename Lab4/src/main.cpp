#include <Arduino.h>
#include <NewPing.h>
#include <LiquidCrystal.h>
#include <DHT.h>
#include <DHT_U.h>

#define DHTPIN 7
#define DHTTYPE DHT11
#define TRIGGERPIN  10
#define ECHOPIN 10
#define MAXDISTANCE 400
#define MINDISTANCE 2

LiquidCrystal lcd(12, 11, 5, 4, 3, 2);
DHT dht(DHTPIN, DHTTYPE);
NewPing sonar(TRIGGERPIN, ECHOPIN, MAXDISTANCE);

void setup() 
{
  dht.begin();
  lcd.begin(16, 2);
}

void print_weather(float hum, float temp)
{
  lcd.clear();
  lcd.print("Temp: ");
  lcd.print(temp);
  lcd.print("C");
  lcd.setCursor(0,1); 
  lcd.print("Hum: ");
  lcd.print(hum);
  lcd.print("%");
  lcd.display();
  delay(2000);
}

void print_distance(float distance)
{
  lcd.clear();
  lcd.print("Distance: ");
  lcd.setCursor(0,1); 
  if (distance >= MAXDISTANCE || distance <= MINDISTANCE) lcd.print("Out of range");
  else lcd.print(distance);
  delay(500);
}

void get_readings(float& hum, float& temp, float& distance)
{
    hum = dht.readHumidity();
    temp = dht.readTemperature();
    float soundms = 331.4 + (0.606 * temp) + (0.0124 * hum);
    float soundcm = soundms/10000;
    float duration = sonar.ping_median(3);
    distance = (duration/2) * soundcm;
}

void loop() 
{
  for(int i=0;i<20;++i)
  {
    float hum, temp, distance;
    get_readings(hum, temp, distance);
    if(i==0)
    {
      print_weather(hum, temp);
    }
    else
    {
      print_distance(distance);
    }
  }
}