#include <Arduino.h>

/*
  Written by Thomas Gadek
  Nov 10 2020

  Program that plays octaves 3-5 on a buzzer
  Pin 9 to Buzzer
*/

const int buzzerPin = 9;

int notes[5][7] = {{33,37,41,44,49,55,62},
                   {65,73,82,87,98,110,123},
                   {131,147,165,175,196,220,247},
                   {262,294,330,349,392,440,494},
                   {523,587,659,698,784,880,988}};


void setup(){
  Serial.begin(9600);
  pinMode(buzzerPin, OUTPUT);
}

void loop(){
  for(int octave=3;octave<=5;++octave)
  {
    for(int note=0;note<7;++note)
    {
      tone(buzzerPin, notes[octave-1][note]);
      delay(700);
      noTone(buzzerPin);
      delay(100);
    }
  }
}