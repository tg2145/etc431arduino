#include <Arduino.h>
#include <Funcs.h>

#include "VARS.h"

/*
 *  etc431lab1.ino
 *  Lab 1, Projects 1 and 2
 *  Thomas Gadek
 *
 *  Program that performs exercise 1 and 2 back to back
 *
 *  Exercise 1
 *  Connect a second LED to the Arduino (don’t forget the 330 Ω resistors) 
 *  and write the code to make both LEDs to turn ON and OFF at unison 
 *  at an interval of half a second.  Use variables.
 * 
 *  Exercise 2
 *  Add  a  third  LED  to  the  project  and  make  it  blink  4  times  at  different  times  off  and  on.
 *  So you will have three LEDs each one blinking different number of times, and at different intervals.
 *
 *  Circuit:
 *    connect red to pin 9
 *    connect yellow to pin 10
 *    connect green to pin 11
 *    330 ohm between leds
 */
  
void setup() 
{
  pinMode(redLEDpin, OUTPUT);
  pinMode(yellowLEDpin, OUTPUT);
  pinMode(greenLEDpin, OUTPUT);
  Serial.begin(9600);
}

void loop() 
{
  //Exercise 1
  Serial.println("Restart");
  doubleBlinkInterval(redLEDpin, yellowLEDpin, halfSecond, 10);
  
  delay(2000); //delay in between exercises
  
  //Exercise 2
  blinkInterval(redLEDpin, redOnTime, redOffTime, redIterations);
  blinkInterval(yellowLEDpin, yellowOnTime, yellowOffTime, yellowIterations);
  blinkInterval(greenLEDpin, greenOnTime, greenOffTime, greenIterations);
 
  delay(2000); //delay in between exercises
}
