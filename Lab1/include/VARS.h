#ifndef VARS_H
#define VARS_H

int redLEDpin = 9;
int redOnTime = 250;
int redOffTime = 150;
int redIterations = 10;
  
int yellowLEDpin = 10;
int yellowOnTime = 500;
int yellowOffTime = 250;
int yellowIterations = 5;

int greenLEDpin = 11;
int greenOnTime = 200;
int greenOffTime = 400;
int greenIterations = 4;
  
int halfSecond = 500;

#endif