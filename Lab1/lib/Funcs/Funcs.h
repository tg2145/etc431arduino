#ifndef FUNCS_H
#define FUNCS_H

void doubleBlinkInterval(int led1, int led2, int interval, int iterations);
void blinkInterval(int led, int onTime, int offTime, int iterations);

#include "Funcs.c"

#endif