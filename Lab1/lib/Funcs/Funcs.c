#include <Arduino.h>

void doubleBlinkInterval(int led1, int led2, int interval, int iterations)
{
  for(int i=0;i<iterations;++i)
  {
    digitalWrite(led1, HIGH);
    digitalWrite(led2, HIGH);
    delay(interval);
    digitalWrite(led1, LOW);
    digitalWrite(led2, LOW);
    delay(interval);
  }
}

void blinkInterval(int led, int onTime, int offTime, int iterations)
{
  for(int i=0;i<iterations;++i)
  {
    digitalWrite(led, HIGH);
    delay(onTime);
    digitalWrite(led, LOW);
    delay(offTime);
  }
}