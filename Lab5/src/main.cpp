#include <Arduino.h>
#include <Servo.h>

const int maxPosition = 175;
const int minPosition = 5;
const int minAnalogReading = 0;
const int maxAnalogReading = 1023;

int servoPosition = 0;
int servoPin = 9;
int servoDelay = 25;

Servo servo;

void setup() {
  Serial.begin(9600);
  servo.attach(servoPin);
}

void loop() {
  //we can use the map function to simplify the angle calculation
  servoPosition = map(analogRead(A0), minAnalogReading, 
                                      maxAnalogReading, 
                                      minPosition, 
                                      maxPosition);
  Serial.println(servoPosition);
  servo.write(servoPosition);
  delay(servoDelay);
}