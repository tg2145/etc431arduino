#include <Arduino.h>

/*

  Written by Thomas Gadek
  Program that cycles through some preset colors of an RGB LED, allows the user to input values for a custom color, and then randomly shifts that color for a few seconds
  Circuit
  Pin 11 to R
  Pin 10 to G
  Pin 9 to B
  220 ohm between each pin and the LED
  10/21/20

*/

typedef struct {
  int colors[3];
} RGBValue;

typedef struct {
  int redPin;
  int greenPin;
  int bluePin;
  int colors[3];
} RGBLED;

RGBValue GRAY = {128,128,128};
RGBValue TEAL = {0,128,128};
RGBValue NAVY = {0,0,128};
RGBValue MAROON = {128,0,0};
RGBValue KHAKY = {240,230,140};
RGBValue MAGENTA = {255,0,255};
RGBValue PINKK = {255,192,203};
RGBValue BLACK = {0,0,0};
RGBValue GOLD = {255,215,0};

RGBLED rgbled = {11,10,9};
String colors[3] = {"red","green","blue"};

void displayRGB(RGBLED* x)
{
  analogWrite(x->redPin, x->colors[0]);
  analogWrite(x->greenPin, x->colors[1]);
  analogWrite(x->bluePin, x->colors[2]);
}

void displayRGBValue(RGBLED* x, RGBValue* y)
{
  analogWrite(x->redPin, y->colors[0]);
  analogWrite(x->greenPin, y->colors[1]);
  analogWrite(x->bluePin, y->colors[2]);
  delay(500);
}

void getRGBInput(RGBLED* x)
{
  for(int i=0;i<3;++i)
  {
    bool valid = false;
    while(!valid)
    {
      Serial.println();
      Serial.print("Enter ");
      Serial.print(colors[i]);
      Serial.print(" value: ");
      while(Serial.available() == 0){}
      x->colors[i] = Serial.parseInt();
      if(x->colors[i] <= 255 && x->colors[i] >= 0) valid = true;
    }
  }
}

void displayRandomRGB(RGBLED* x)
{
  for(int i=0;i<2000;++i)
  {
    for(int j=0;j<3;++j)
    {
      x->colors[j] += rand()%9-4;
      if(x->colors[j] > 255)
      {
        x->colors[j] = 2 * 255 - x->colors[j];
      }
      if(x->colors[j] < 0)
      {
        x->colors[j] = -1 * x->colors[j];
      }
    }
    Serial.println();
    displayRGB(x);
    delay(5);
  }
}

void displayColorPalette(RGBLED* x)
{
  displayRGBValue(x, &GRAY);
  displayRGBValue(x, &TEAL);
  displayRGBValue(x, &NAVY);
  displayRGBValue(x, &MAROON);
  displayRGBValue(x, &KHAKY);
  displayRGBValue(x, &MAGENTA);
  displayRGBValue(x, &PINKK);
  displayRGBValue(x, &BLACK);
  displayRGBValue(x, &GOLD);
}

void setup() {
  Serial.begin(9600);
  pinMode(rgbled.redPin, OUTPUT);
  pinMode(rgbled.greenPin, OUTPUT);
  pinMode(rgbled.bluePin, OUTPUT);
}

void loop() {
  displayColorPalette(&rgbled);
  getRGBInput(&rgbled);
  displayRGB(&rgbled);
  delay(1000);
  displayRandomRGB(&rgbled);
}