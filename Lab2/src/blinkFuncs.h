/*
 *  Lab2/src/blinkFuncs.h
 * 
 *  Thomas Gadek
 * 
 *  Header file for several blink functions needed for Lab 2
 */

#ifndef BLINKFUNCS_H
#define BLINKFUNCS_H

#ifdef __cplusplus
extern "C" {
#endif

#include <Arduino.h>

typedef struct 
{
  int pin;
  int onTime;
  int offTime;
  int iterations;
  float voltage;
} BlinkIntervalCfg;

void setBlinkIntervalCfg(BlinkIntervalCfg* cfg, int pin, int onTime, int offTime, int iterations, float voltage);
void blinkIntervalDigital(BlinkIntervalCfg* cfg);
void blinkIntervalAnalog(BlinkIntervalCfg* cfg);

#ifdef __cplusplus
}
#endif

#endif