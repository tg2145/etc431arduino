#include <Arduino.h>
#include <math.h>

#include "VARS.h"
#include "blinkFuncs.h"
#include "serialFuncs.hpp"

/*
 *  Lab2/src/main.cpp
 *  Lab 2, Projects 3 and 4
 *  Thomas Gadek
 *
 *  Program that performs exercise 3 and 4 back to back
 *
 *  Exercise 3
 *  Add user input for the blink iterations for the red and yellow LED
 * 
 *  Exercise 4
 *  Change the blink exercise to analog
 *  Add user input for all variables of the blink program
 *
 *  Circuit:
 *    connect red to pin 9
 *    connect yellow to pin 10
 *    330 ohm between leds
 */
  
void setup() 
{
  pinMode(redLEDpin, OUTPUT);
  pinMode(yellowLEDpin, OUTPUT);
  Serial.begin(9600);
}

void loop() 
{
  Serial.println("Restart");

  //Exercise 3
  BlinkIntervalCfg redLED, yellowLED;
  int inputRedIterations = getIterationsInput("red");
  int inputYellowIterations = getIterationsInput("yellow");
  setBlinkIntervalCfg(&redLED, redLEDpin, redOnTime, redOffTime, inputRedIterations, 0);
  setBlinkIntervalCfg(&yellowLED, yellowLEDpin, yellowOnTime, yellowOffTime, inputYellowIterations, 0);
  blinkIntervalDigital(&redLED);
  blinkIntervalDigital(&yellowLED);
 
  delay(2000); //delay in between exercises

  //Exercise 4
  redLED.pin = redLEDpin;
  yellowLED.pin = yellowLEDpin;
  getBlinkIntervalCfgInput(&redLED, "red");
  getBlinkIntervalCfgInput(&yellowLED, "yellow");
  blinkIntervalAnalog(&redLED);
  blinkIntervalAnalog(&yellowLED);

  delay(2000); //delay in between exercises
}
