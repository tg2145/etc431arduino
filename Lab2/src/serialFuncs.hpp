/*
 *  Lab2/src/serialFuncs.hpp
 *
 *  Thomas Gadek
 * 
 *  Header file for two functions used to obtain input from the serial interface
 */

#ifndef SERIALFUNCS_HPP
#define SERIALFUNCS_HPP

#include <Arduino.h>
#include "blinkFuncs.h"
#include "VARS.h"

int getIterationsInput(String color);
void getBlinkIntervalCfgInput(BlinkIntervalCfg* cfg, String color);

#endif