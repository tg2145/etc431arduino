/*
 *  Lab2/include/VARS.h
 * 
 *  Thomas Gadek
 * 
 *  Header file that defines several global variables needed for Lab 2
 */

#ifndef VARS_H
#define VARS_H

const int redLEDpin = 9;
const int redOnTime = 250;
const int redOffTime = 150;
const int redIterations = 10;
   
const int yellowLEDpin = 10;
const int yellowOnTime = 500;
const int yellowOffTime = 250;
const int yellowIterations = 5;

const int greenLEDpin = 11;
const int greenOnTime = 200;
const int greenOffTime = 400;
const int greenIterations = 4;

const int halfSecond = 500;

const float MAXVOLTAGE = 5.0;

#endif