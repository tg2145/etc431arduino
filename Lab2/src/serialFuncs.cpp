#include "serialFuncs.hpp"

/*  Lab2/src/serialFuncs.cpp
 *
 *  Thomas Gadek
 * 
 *  Source file for two functions used to obtain input from the serial interface
 */

int getIterationsInput(String color)
{
  Serial.println();
  Serial.print("How many times do you want the ");
  Serial.print(color);
  Serial.print(" LED to blink: ");
  while(Serial.available() == 0){}
  return Serial.parseInt();
}

void getBlinkIntervalCfgInput(BlinkIntervalCfg* cfg, String color)
{
  Serial.println();
  Serial.print("Set ");
  Serial.print(color);
  Serial.print(" LED blink intervals: ");
  while(Serial.available() == 0){}
  cfg->iterations = Serial.parseInt();

  Serial.println();
  Serial.print("Set ");
  Serial.print(color);
  Serial.print(" LED on time in ms: ");
  while(Serial.available() == 0){}
  cfg->onTime = Serial.parseInt();

  Serial.println();
  Serial.print("Set ");
  Serial.print(color);
  Serial.print(" LED off time in ms: ");
  while(Serial.available() == 0){}
  cfg->offTime = Serial.parseInt();

  Serial.println();
  Serial.print("Set ");
  Serial.print(color);
  Serial.print(" LED voltage: ");
  while(Serial.available() == 0){}
  cfg->voltage = fmod(Serial.parseFloat(), MAXVOLTAGE);
}