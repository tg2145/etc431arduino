#include "blinkFuncs.h"

/*
 *  Lab2/src/blinkFuncs.c
 * 
 *  Thomas Gadek
 * 
 *  Source file for the several blink functions needed for Lab 2
 */

void setBlinkIntervalCfg(BlinkIntervalCfg* cfg, int pin, int onTime, int offTime, int iterations, float voltage)
{
  cfg->pin = pin;
  cfg->onTime = onTime;
  cfg->offTime = offTime;
  cfg->iterations = iterations;
  cfg->voltage = voltage;
}

void blinkIntervalDigital(BlinkIntervalCfg* cfg)
{
  for(int i=0;i<cfg->iterations;++i)
  {
    digitalWrite(cfg->pin, HIGH);
    delay(cfg->onTime);
    digitalWrite(cfg->pin, LOW);
    delay(cfg->offTime);
  }
}

void blinkIntervalAnalog(BlinkIntervalCfg* cfg)
{
  float SCALEVOLTS = 51;
  for(int i=0;i<cfg->iterations;++i)
  {
    analogWrite(cfg->pin, (int)cfg->voltage*SCALEVOLTS);
    delay(cfg->onTime);
    analogWrite(cfg->pin, 0);
    delay(cfg->offTime);
  }
}