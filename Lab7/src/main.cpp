#include <Arduino.h>

/*
  Written by Thomas Gadek
  Nov 23 2020

  Program that controls LEDs based on the position of an analog joystick
  Top joystick position lights up top LED
  Right joystick position lights up right LED
  Bottom joystick position lights up left LED
  Left joystick position lights up bottom LED
  As seen in the video

  Pin A0 to Vx
  Pin A1 to Vy

  Pin 10 to topLed
  Pin 11 to botLed
  Pin 12 to leftLed
  Pin 13 to rightLed
  All connected to a 220Ohm resistor that is connected to GND
  Picture of circuit supplied with program
*/

#define joyX A0
#define joyY A1
#define topLedPin 10
#define botLedPin 11
#define leftLedPin 12
#define rightLedPin 13

typedef struct {
  int xValue;
  int yValue;
} AnalogJoystick;

AnalogJoystick analogJoystick;

void readAnalogJoystick(AnalogJoystick* a)
{ 
  a->xValue = analogRead(joyY); //flip because my
  a->yValue = analogRead(joyX); //joystick is sideways
}

void processLeds(AnalogJoystick* a)
{
  //reset all lights
  digitalWrite(topLedPin, LOW);
  digitalWrite(botLedPin, LOW);
  digitalWrite(leftLedPin, LOW);
  digitalWrite(rightLedPin, LOW);
  //If x >0 and y <=10 (between 0 and 10), turn on the left LED
  if(a->xValue > 0 && a->yValue <= 10)
  {
    digitalWrite(leftLedPin, HIGH);
  }
  //If x <=10 (between 0 and 10) and y >=0, turn on the lower LED 
  else if(a->xValue <= 10 && a->yValue >= 0)
  {
    digitalWrite(botLedPin, HIGH);
  }
  //If x >= 1020 (between 1020 and 1023) and Y >=500 (between 500 and 511), turn on the right LED
  else if(a->xValue >= 1020 && a->yValue >= 500 && a->yValue <= 511)
  {
    digitalWrite(rightLedPin, HIGH);
  }
  //If x >=500 (between 500 and 511) and y >=1-20 (between 1020 and 1023), turn on the top LED.
  else if(a->xValue >= 500 && a->xValue <= 511 && a->yValue >= 1020)
  {
    digitalWrite(topLedPin, HIGH);
  }
}

void setup() {
  pinMode(topLedPin, OUTPUT);
  pinMode(botLedPin, OUTPUT);
  pinMode(leftLedPin, OUTPUT);
  pinMode(rightLedPin, OUTPUT);
}

void loop() {
  readAnalogJoystick(&analogJoystick);
  processLeds(&analogJoystick);
  delay(20);
}