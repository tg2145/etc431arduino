#include <Arduino.h>
#include <NewPing.h>
#include <LiquidCrystal.h>
#include <DHT.h>
#include <DHT_U.h>

/*
  Written by Thomas Gadek
  Program that uses a DHT sensor and Echo sensor to measure an accurate distance.
  It displays the distance in CM on a 16x2 lcd display.
  It writes a voltage to an LED to represent distance values between 10 and 60cm. 
  12/7/20

  Circuit:
    Pin 13 to LED with 220 ohm resistor
    Pin 7 to DHT Sensor
    Pin 10 to echo sensor
    Pins 12, 11, 5, 4, 3, 2 to the Liquid Crystal Display
*/

#define DHTPIN 7
#define DHTTYPE DHT11
#define TRIGGERPIN  10
#define ECHOPIN 10
#define MAXDISTANCE 400
#define MINDISTANCE 2
#define LEDPIN 13

LiquidCrystal lcd(12, 11, 5, 4, 3, 2);
DHT dht(DHTPIN, DHTTYPE);
NewPing sonar(TRIGGERPIN, ECHOPIN, MAXDISTANCE);

void setup() 
{
  pinMode(LEDPIN, OUTPUT);
  dht.begin();
  lcd.begin(16, 2);
}

void print_distance(float distance)
{
  lcd.clear();
  lcd.print("Distance: ");
  lcd.setCursor(0,1); 
  if (distance >= MAXDISTANCE || distance <= MINDISTANCE) lcd.print("Out of range");
  else lcd.print(distance);
}

void display_led(float distance)
{
  if(distance > 60 || distance < 10) //if not in led range
  {
    analogWrite(LEDPIN, 0); //reset led
    return; //and exit
  }
  float temp = distance - 10; // 10-60cm is normalized to 0-50
  float floatVoltage = temp/10; //0-50 is scaled to 0-5V
  int bitVoltage = floatVoltage*51; //scale to 8 bit value for arduino 0-5.0 -> 0-255
  analogWrite(LEDPIN, bitVoltage);
}

void get_readings(float& hum, float& temp, float& distance)
{
  hum = dht.readHumidity();
  temp = dht.readTemperature();
  float soundms = 331.4 + (0.606 * temp) + (0.0124 * hum);
  float soundcm = soundms/10000;
  float duration = sonar.ping_median(3);
  distance = (duration/2) * soundcm;
}

void loop() 
{
  float hum, temp, distance;
  get_readings(hum, temp, distance);
  print_distance(distance);
  display_led(distance);
  delay(500);
}