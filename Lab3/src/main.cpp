#include <Arduino.h>
#include <NewPing.h>
#include <DHT.h>
#include <DHT_U.h>
#include <Adafruit_AM2315.h>

#include <stdio.h>

#define DHTPIN 7
#define DHTTYPE DHT11
#define TRIGGERPIN  10
#define ECHOPIN 10
#define MAXDISTANCE 400
#define MINDISTANCE 2

NewPing sonar(TRIGGERPIN, ECHOPIN, MAXDISTANCE);
DHT dht(DHTPIN, DHTTYPE);

void setup() {
  Serial.begin(9600);
  dht.begin();
}

void loop() {
  delay(2000);

  int iterations = 5;
  float hum = dht.readHumidity();
  float temp = dht.readTemperature();

  float soundms = 331.4 + (0.606 * temp) + (0.0124 * hum);
  float soundcm = soundms/10000;
  float duration = sonar.ping_median(iterations);
  float distance = (duration/2) * soundcm;

  Serial.print("Sound: ");
  Serial.print(soundms);
  Serial.print("m/s, Humidity: ");
  Serial.print(hum);
  Serial.print("%, Temp: ");
  Serial.print(temp);
  Serial.print("C, Distance: ");
  if (distance >= MAXDISTANCE || distance <= MINDISTANCE) Serial.print("Out of range");
  else Serial.print(distance);
  Serial.println();
}